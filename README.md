# PinyinIME

#### 介绍
基于googlepinyin输入法改造

#### 软件架构
软件架构说明
AndroidStudio3.4.1

Gradle plugin3.4.1

Gradle 5.1.1

NDK 19.2.5345600 & CMake 3.6.4111459

ANDROID_HOME 安卓SDK系统环境变量根基自己定义修改，在app/build.gradle
layoutlib.jar 采用的是android-22 在app/build.gradle根基系统自己修改

#### 效果图
##### 竖屏
<img src="https://gitee.com/jabony/PinyinIME/raw/master/screenshot/选择默认输入法.png" width = "300" height = "533" />
<img src="https://gitee.com/jabony/PinyinIME/raw/master/screenshot/默认全键盘输入法.png" width = "300" height = "533" />
<img src="https://gitee.com/jabony/PinyinIME/raw/master/screenshot/输入中文效果.png" width = "300" height = "533" />

##### 横屏
<img src="https://gitee.com/djyan/PinyinIME/raw/master/screenshot/中文输入界面.png" width = "512" height = "384" />
<img src="https://gitee.com/djyan/PinyinIME/raw/master/screenshot/英文输入界面.png" width = "512" height = "384" />


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 博客
https://blog.csdn.net/jabony/article/details/90289419

#### PinyinIME
google 拼音输入法 android studio可编译


使用带hide的android.jar进行编译,见https://github.com/anggrayudi/android-hidden-api


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)